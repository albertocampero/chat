// agrega un cero a la izquierda
function AddZero(num) {
    return (num >= 0 && num < 10) ? "0" + num : num + "";
}
// devuelve la fecha y hora actual
function getDateTime() {
    var now = new Date();
    var strDateTime = [
        [
            AddZero(now.getDate()),
            AddZero(now.getMonth() + 1),
            now.getFullYear()].join("/"),
            [
                AddZero(now.getHours()), 
                AddZero(now.getMinutes()),
                AddZero(now.getSeconds())
            ].join(":")
        ].join(" ");
        return strDateTime;
};
// obtiene el valor de las variables de la URL
function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
// obtiene los contactos asignados al vendedor
function getContacts(referencia) {
    referencia.orderByChild("name").on("child_added", function(data) {
        $('<a/>', {
            class: 'list-group-item',
            href: '?contact='+data.val().id,
            html: '<div class="media"><div class="media-left"><img class="img-circle" src="images/avatar.jpg" alt=""></div><div class="media-body"><h5>'+data.val().name+'</h5></div></div>'
        }).appendTo('#list-contact');
    });
}
/* filtrado de contactos*/
jQuery.expr[':'].Contains = function(a,i,m){
    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
};
$('#search').change(function() {
    var filter = $(this).val();
    var cont = $('#list-contact');
    if(filter) {
        $(cont).find("a:not(:Contains(" + filter + "))").slideUp();
        $(cont).find("a:Contains(" + filter + ")").slideDown();
    }else {
        $(cont).find("a").slideDown();
    }
    return false;
}).keyup( function () {
    $(this).change();
});
// autenticación en tiempo real
firebase.auth().onAuthStateChanged(function(firebaseUser){
    // si no esta logeado redirigirt al login
    if (!firebaseUser) {
        window.location = 'login.html';
    }else {

        // trae la información del usuario de la sesión activa
        var sesRef = firebase.database().ref('vrummchat/users/'+firebaseUser.uid);
        sesRef.on("value", function(data) {
            $('#session-data span').html(data.val().name);

            if ( data.val().role == 1 ) {

                $('#container-single').remove();
                $('#container-main').removeClass('hidden');
                // devuelve los contactos del usuario con sesión activa
                var contactsRef = firebase.database().ref('vrummchat/seller-contacts/'+firebaseUser.uid);
                getContacts(contactsRef);
            }else {

                $("#header, #messages-history, #send-form").appendTo("#col-main");
                $('#container-main').remove();
                $('#container-single').removeClass('hidden');
            }
        });
        // trae la información del contacto seleccionado
        var contact = getParameterByName('contact');
        if ( contact ) {

            if ( $(window).width() < 768 ){
                $('#col-left').hide();
                $('#chat-history').show();
                $('#messages-history').addClass('aux-history');
            }

            // trae la información del contacto seleccionado
            var usrRef = firebase.database().ref('vrummchat/users/'+contact);
            usrRef.on("value", function(data) {
                $('#contact-name').html(data.val().name);
                $('#header-contact img').addClass('visible');
            });

            //referencia a los mensajes del contacto
            var chatRef = firebase.database().ref('vrummchat/messages/'+contact);            
        }else {
            //referencia a los mensajes del usuario activo
            var chatRef = firebase.database().ref('vrummchat/messages/'+firebaseUser.uid);
        }

        chatRef.on("child_added", function(data) {
            $('<li/>', {
                class: 'list-group-item',
                html: '<div class="media"><div class="media-left"><img class="img-circle" src="images/avatar.jpg" alt=""></div><div class="media-body"><h4 class="pull-left">'+data.val().name+'</h4><small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span> '+data.val().timestamp+'</small><div class="clearfix"></div><p>'+data.val().message+'</p></div></div>'

            }).appendTo('#messages-history');
            $('#messages-history').animate({ scrollTop: $('#messages-history').prop('scrollHeight')}, 1);
        });
        //envia el mensaje
        $('#send-form').on('submit', function() {
            var text = $('#btn-input').val();
            var user = $('#session-data span').html();
            var date = getDateTime();
            const promise = chatRef.push({
                message: text,
                name: user,
                iduser: firebaseUser.uid,
                timestamp: date
            });
            promise.then( function(snapshot) {
                $('#btn-input').val('');
            }).catch( function(e) {
                alert(e.message);
            });
            return false;
        });
    }
});
// logout
$('#logout').on('click', function(e) {
    e.preventDefault()
    firebase.auth().signOut();
});
$("#messages-history").animate({ scrollTop: $(document).height() }, "slow");